import pymongo
import psutil

def get_database_record():
    cpu = []
    name = []
    status = []

    myclient = pymongo.MongoClient("mongodb+srv://dronescan:3ratcat3@cluster0-rools.mongodb.net/tenants?retryWrites=true&w=majority")
    db = myclient["Edge_Device_Status"]
    # print("Connected successfully!!!")
    posts=db.Connection_List

    for connection_list in posts.find():
        name.append(connection_list['Name'])
        cpu.append(connection_list['CPU'])
        status.append(connection_list['Status'])

    return name,cpu,status 

def getpriority():
    myclient = pymongo.MongoClient("mongodb+srv://dronescan:3ratcat3@cluster0-rools.mongodb.net/tenants?retryWrites=true&w=majority")
    db = myclient["Priority"]
    posts=db.Connection_List

    for connection_list in posts.find().sort("_id", -1):
        pri_device = connection_list['Device_Name']
        priority_data = connection_list['Priority']
        break
    return pri_device,priority_data

def update_device_status(slave_Device_Name):
    myclient = pymongo.MongoClient("mongodb+srv://dronescan:3ratcat3@cluster0-rools.mongodb.net/tenants?retryWrites=true&w=majority")
    db = myclient["Edge_Device_Status"]
    # print("Connected successfully!!!")
    posts=db.Connection_List

    cpu = psutil.cpu_percent(percpu=True)
    cpu_usage = round((sum(cpu)/len(cpu)),2)
    
    new_val_status = {"$set": {"Status": "online"}}
    new_val_cpu = {"$set": {"CPU": cpu_usage}}
    filter1 = {"Name":slave_Device_Name}
    posts.update_many(filter1, new_val_status)
    posts.update_many(filter1, new_val_cpu)
    # print("Data Updated")

def create_db(slave_Device_Name):
    cpu = []
    name = []
    status = []
    
    myclient = pymongo.MongoClient("mongodb+srv://dronescan:3ratcat3@cluster0-rools.mongodb.net/tenants?retryWrites=true&w=majority")
    db = myclient["Edge_Device_Status"]
    posts=db.Connection_List
    # print("Connected successfully!!!")
    for connection_list in posts.find():
        name.append(connection_list['Name'])
        cpu.append(connection_list['CPU'])
        status.append(connection_list['Status'])

    if slave_Device_Name not in name:
        mydict = { "Name": slave_Device_Name, "Status": "online","CPU":55 }
        x = posts.insert_one(mydict)
    else:
        new_val_status = {"$set": {"Status": "online","CPU":55}}
        filter1 = {"Name":slave_Device_Name}
        posts.update_many(filter1, new_val_status)



def update_master_status():
    myclient = pymongo.MongoClient("mongodb+srv://dronescan:3ratcat3@cluster0-rools.mongodb.net/tenants?retryWrites=true&w=majority")
    db = myclient["Edge_Device_Status"]
    # print("Connected successfully!!!")
    posts=db.Connection_List
    new_val_status = {"$set": {"Status": "offline"}}
    posts.update_many({}, new_val_status)
    # print("Data Updated")