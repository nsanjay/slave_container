#########################################################################################
# This program facilitates master device management based on the device type,
# create master MQTT communication channel and publish data to a locally hosted database.

# Usage : python3 master.py
#########################################################################################

import os
import pymongo
import paho.mqtt.publish as publish
import paho.mqtt.subscribe as subscribe
import psutil
import time
from threading import Thread
import json
import subprocess as sp

from config import config
from get_device_records import get_record


def send_status():
    while True:
        data = {"Message":"Master"}
        data = json.dumps(data)
        publish.single("slave_topic", data, hostname="mqtt.eclipse.org")
        time.sleep(1)

def check_offline_device():
    true_offline = []
    while True:
        offline_device = []
        online_device = []
        device_cpu = []
        get_record.update_master_status()
        data = {"Message":"Update_Status"}
        data = json.dumps(data)
        publish.single("master_topic", data, hostname="mqtt.eclipse.org")
        time.sleep(5)
        publish.single("master_topic", data, hostname="mqtt.eclipse.org")
        time.sleep(3)
        publish.single("master_topic", data, hostname="mqtt.eclipse.org")
        time.sleep(3)

        name,cpu,status = get_record.get_database_record()
        os.system("echo " + str(name) + " " + str(cpu) + " " + str(status))

        for i in range(len(status)):
            if status[i] == "offline":
                offline_device.append(name[i])
            else:
                online_device.append(name[i])
                device_cpu.append(cpu[i])

        os.system("echo " + str(offline_device) + " " + str(online_device) + " " + str(device_cpu))

        for i in range(len(device_cpu)):
            if device_cpu[i] > 90:
                os.system("echo " + "Maximum CPU Utilization Edge Station: " + str(online_device[i]))

        already_handled = set(true_offline).intersection(offline_device)

        os.system("echo " + "Offline Device " + str(offline_device))
        for i in already_handled:
            offline_device.remove(i)
        os.system("echo " + "New offline devices to be handled: " + str(offline_device))

        back_online = set(true_offline).intersection(online_device)

        for i in back_online:
            sp.Popen.terminate(extProc)
            true_offline.remove(i)

        for i in offline_device:
            true_offline.append(i)
            extProc = sp.Popen(['python3','device_management/demo.py'])
            

os.system("echo 'Starting Master MQTT Communication...'")

thread = Thread(target = send_status)
thread.start()
thread1 = Thread(target = check_offline_device)
thread1.start()

while True:
    if not thread.is_alive():
        thread = Thread(target = send_status)
        thread.start()
    if not thread1.is_alive():
        thread1 = Thread(target = check_offline_device)
        thread1.start()