#!/bin/bash

mongoimport --host mongodb --db Master --collection MQTT_Topics --type json --file mqtt.json --jsonArray

mongoimport --host mongodb --db Slave --collection Configuration --type json --file master_config.json --jsonArray

mongoimport --host mongodb --db Slave --collection MQTT_Topics --type json --file mqtt.json --jsonArray